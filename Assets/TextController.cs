﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TextController : MonoBehaviour {

	public Text text;

    private enum States { cela, lustro, posciel_0, zamek_0, lustro_cela, posciel_1, zamek_1, wolnosc};

	// Use this for initialization
	void Start () {
		text.text = "Witam w mojej pierwszej tekstowej grze przygodowej." + 
			" Ta gra będzie polegać na ucieczce z więzienia, oraz na nauce programowania w C# poprzez pisanie kodu gry." +
				" To jest moja pierwsza gra więc proszę o wyrozumiełość ;)." + 
				" W miare postępu w nauce będę gre uzupełniał.";
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void state_cell ()
    {
        text.text = "Jesteś w więziennej celi i chcesz z niej uciec." +
            "Na łóżku leży brudna pościel,na ścanie wisi potłuczone" +
            " lustro oraz drzwi które są zamknięte od zewnątrz.\n\n" +
            "Wciśnij P żeby zobaczyć Pościel,L aby zobaczyć Lustro oraz Z aby zobaczyć Zamek.";
        if (Input.GetKeyDown(KeyCode.P))
        {

        }

    }
}
